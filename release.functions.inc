<?php

/**
 * Enables and than installs any number of modules.
 *
 * @param $modules
 *   An array containing a list of the module machine readable names.
 */
function release_enable_modules($modules = array()) {
  include_once './includes/install.inc';
  module_rebuild_cache();
  module_enable($modules);
  drupal_install_modules($modules);
}

/**
 * Disables and than uninstalls any number of modules.
 *
 * @param $modules
 *   An array containing a list of the module machine readable names.
 */
function release_disable_modules($modules = array()) {
  include_once './includes/install.inc';
  module_rebuild_cache();
  module_disable($modules);
  foreach ($modules as $module) {
    drupal_uninstall_module($module);
  }
}

/**
 * Updates permissions for a specific role.
 *
 * @param $role
 *   Numerical ID indicating the role.
 * @param $add_perms
 *   Array containing a list of permissions to add to the role.
 * @param $del_perms
 *   Array containing a list of permissions to remove from the role.
 */
function release_update_permissions($role, $add_perms, $del_perms = array()) {
  $permissions = explode(', ', db_result(db_query("SELECT perm FROM {permission} WHERE rid = %d", $role)));
  
  // Add new permissions into the array.
  if (is_array($add_perms) && count($add_perms)) {
    foreach ($add_perms as $perm) {
      if (!in_array($perm, $permissions)) {
        $permissions[] = $perm;
      }
    }
  }
  
  // Remove existing permissions from the array.
  if (is_array($del_perms) && count($del_perms)) {
    foreach ($del_perms as $perm) {
      if ($key = array_search($perm, $permissions)) {
        unset($permissions[$key]);
      }
    }
  }
  
  db_query("UPDATE {permission} SET perm = '%s' WHERE rid = %d", implode(', ', $permissions), $role);
}

/**
 * Creates a simple node of any type specified.
 *
 * @param $type
 *   String indicating the content type.
 * @param $title
 *   Title of the new node.
 * @param $body
 *   Body of the new node.
 * @param $path
 *   Optional URL alias to save in the new node.
 */
function release_create_node($type, $title, $body, $path = NULL) {
  global $user;
  
  $node = new StdClass();
	$node->type = $type;
	$node->status = 1;
	$node->title = $title;
	$node->body = $body;
	$node->teaser = node_teaser($body);
	$node->uid = $user->uid;
	$node->comment = variable_get('comment_'. $type, 0);
	
	if (!empty($path)) {
	  $node->pathauto_perform_alias = '0';
	  $node->path = $path;
	}
	
	node_save($node);
}

/**
 * Makes changes to a specific block.
 *
 * @param $module
 *   String indicating the module that created the block.
 * @param $delta
 *   String indicating the delta or ID of the block.
 * @param $changes
 *   An associative array containing the changes for the block.
 * @param $theme
 *   Alternatively define the theme to make the change on, default is current theme.
 */
function release_update_block($module, $delta, $changes, $theme = NULL) {
  _block_rehash();
  
  // Categorize the changes into the appropiate tables.
  $block_changes = array();
  $box_changes = array();
  foreach ($changes as $type => $value) {
    switch ($type) {
      case 'status':
      case 'weight':
      case 'region':
      case 'custom':
      case 'throttle':
      case 'visibility':
      case 'pages':
      case 'title':
        $block_changes[] = $type .' = "'. addslashes($value) .'"';
        break;
        
      case 'body':
      case 'info':
      case 'format':
        $box_changes[] = $type .' = "'. addslashes($value) .'"';
        break;
    }
  }
  
  // Execute changes on the block.
  if (count($block_changes)) {
    $change_set = implode(', ', $block_changes);
    db_query("UPDATE {blocks} SET {$change_set} WHERE module = '%s' AND delta = '%s'", $module, $delta);
  }
  
  // Execute changes on the box. This only pertains to blocks which were manually
  // created using the block module UI.
  if (count($box_changes) && is_numeric($delta)) {
    $change_set = implode(', ', $box_changes);
    db_query("UPDATE {boxes} SET {$change_set} WHERE bid = %d", $delta);
  }
}